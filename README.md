# goathlib

OTP generator library compatible with Google Authenticator.

Original code was taken from https://github.com/reedobrien/goathtool
and then rewritten.
