package goathlib

import (
	"fmt"
	"time"
)

func ExampleGenerateHOTP() {
	key, err := ParseSecret("GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ")
	if err != nil {
		panic(err)
	}
	fmt.Println(GenerateHOTP(key, 0))
	fmt.Println(GenerateHOTP(key, 1))
	fmt.Println(GenerateHOTP(key, 2))
	// Output:
	// 755224
	// 287082
	// 359152
}

func ExampleGenerateTOTP() {
	// goathtool totp  -b -N "2005-03-18 01:58:29 UTC" 'gr6d 5br7 25s6 vnck v4vl hlao re'
	key, err := ParseSecret("gr6d 5br7 25s6 vnck v4vl hlao re")
	if err != nil {
		panic(err)
	}
	layout := "2006-01-02 15:04:05 MST"
	timeStr := "2005-03-18 01:58:29 UTC"
	t, err := time.Parse(layout, timeStr)
	if err != nil {
		panic(err)
	}
	fmt.Println(GenerateTOTP(key, t))
	// Output: 018108
}
