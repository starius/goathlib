package goathlib

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"fmt"
	"strings"
	"time"
)

const (
	digits = 6
	format = "%06d"
	step   = 30 // Seconds.
)

// ParseSecret parses OTP secret and returns a key that can be passed to
// GenerateHOTP and GenerateTOTP.
func ParseSecret(secret string) ([]byte, error) {
	secret = strings.ToUpper(secret)
	secret = strings.Replace(secret, " ", "", -1)
	return base32.StdEncoding.WithPadding(base32.NoPadding).DecodeString(secret)
}

// GenerateHOTP returns counter based code for given counter value.
func GenerateHOTP(key []byte, counter int64) string {
	hash := hmac.New(sha1.New, key)
	if err := binary.Write(hash, binary.BigEndian, counter); err != nil {
		panic(err)
	}
	h := hash.Sum(nil)

	offset := h[19] & 0x0f
	trunc := binary.BigEndian.Uint32(h[offset : offset+4])
	trunc &= 0x7fffffff

	base := uint32(1)
	for i := 0; i < digits; i++ {
		base *= 10
	}

	code := trunc % base
	return fmt.Sprintf(format, code)
}

// GenerateTOTP returns time based code for given time.
func GenerateTOTP(key []byte, now time.Time) string {
	counter := now.Unix() / step
	return GenerateHOTP(key, counter)
}
